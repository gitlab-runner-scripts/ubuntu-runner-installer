#!/bin/bash

[ `whoami` = root ] || { sudo "$0" "$@"; exit $?; }

echo "1. Stop running docker containers"
docker stop $(docker ps -aq)

echo "2. Remove all docker containers"
docker rm $(docker ps -aq)

echo "3. Remove all docker volumes"
docker volume rm $(docker volume ls -q)

echo "4. Clear memory cache"
sync; echo 3 > /proc/sys/vm/drop_caches
