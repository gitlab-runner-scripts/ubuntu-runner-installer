#!/bin/bash

[ `whoami` = root ] || { sudo "$0" "$@"; exit $?; }

installDocker() {
if which docker &> /dev/null; then
	echo "##########      Docker  installed      ##########"
else
	echo "##########      Installing Docker      ##########"
	apt install docker.io
	systemctl start docker
	systemctl enable docker
	docker --version
	groupadd docker
	gpasswd -a $USER docker
	usermod -aG docker $USER
	chmod 777 /var/run/docker.sock
	echo "##########      Docker  installed      ##########"
fi
}

installGitlabRunner() {
if which gitlab-runner &> /dev/null; then
        echo "##########   gitlab-runner installed    ##########"
else
	echo "##########   Installing gitlab-runner   ##########"
	wget -q -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
	chmod +x /usr/local/bin/gitlab-runner
	gitlab-runner install --user=root --working-directory=/root
	gitlab-runner --version
	gitlab-runner start
	echo "##########   gitlab-runner installed    ##########"
fi
}

echo " "
echo " "
echo "##########       Update Ubuntu        ##########"
apt-get update

echo " "
echo " "
installDocker


echo " "
echo " "
installGitlabRunner

echo " "
echo " "
echo "#########         Create runner       ##########"

echo " "
echo "->  Please enter gitlab url  (like https://gitlab.com/ )"
read GITLAB_URL

echo " "
echo "-> Please enter gitlab runner registration token"
read GITLAB_TOKEN

echo " "
echo "-> Please enter gitlab runner tag (like test or automation-tests)"
read GITLAB_TAG

sudo gitlab-runner register \
--non-interactive \
--description $HOSTNAME \
--url $GITLAB_URL  \
--registration-token $GITLAB_TOKEN \
--executor docker \
--env DOCKER_DRIVER=overlay2 \
--docker-tlsverify=false \
--docker-image docker:19.03.8 \
--docker-privileged \
--docker-disable-entrypoint-overwrite=false \
--docker-oom-kill-disable=false \
--docker-disable-cache=false \
--docker-volumes /cache:/cache \
--docker-volumes /var/run/docker.sock:/var/run/docker.sock \
--docker-volume-driver overlay2 \
--docker-cache-dir /cache \
--docker-network-mode host \
--docker-pull-policy if-not-present \
--tag-list $GITLAB_TAG

gitlab-runner verify

echo "Docker cleanup script defining as a cron"

cp $PWD/docker-cleanup-cron.sh /usr/local/bin/docker-cleanup-cron.sh
touch /usr/local/bin/docker-cleanup-cron.logs
chmod 777 /usr/local/bin/docker-cleanup-cron.sh
chmod 777 /usr/local/bin/docker-cleanup-cron.logs
(crontab -l 2>/dev/null; echo "0 3 1-10 1-10 0-6 bash /usr/local/bin/docker-cleanup-cron.sh > /usr/local/bin/docker-cleanup-cron.logs") | crontab -
crontab -l

echo "Runner is ready to use"
